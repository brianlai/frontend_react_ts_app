// for local dev with webpack-dev-server
// export const hostName = "localhost";

// for AWS deployment (port 80)
export const hostName = "18.163.195.233";

export const frontendBasePath = "";
// export const frontendBasePath = "/app"

// export const frontendPort = 8002;
export const frontendPort = 80;
export const backendPort = 80;

// for local dev with webpack-dev-server
export const frontendBaseUrl = `http://${hostName}${frontendPort !==80 ? ":" + frontendPort: ""}${frontendBasePath}`;
export const backendGraphQlUrl = `http://${hostName}${backendPort !==80 ? ":" + backendPort: ""}/graphql`;

// for dev deploying to expressjs (port 80)
// export const frontendBaseUrl = `http://localhost${frontendBasePath}`;
// export const backendGraphQlUrl = "http://localhost/graphql";

// for AWS deployment (port 80)
// export const frontendBaseUrl = `http://18.163.195.233${frontendBasePath}`;
// export const backendGraphQlUrl = `http://18.163.195.233/graphql`;
