/**
 * Polyfill for backward compability
 */
//for ie11, 
require('isomorphic-fetch');//no fetch
require('es6-promise').polyfill(); //missing Promise
require('es6-object-assign').polyfill(); //missing Object.assign

//Object doesn't support property or method 'includes'
//https://github.com/facebook/create-react-app/issues/2856#issuecomment-317961280
// import 'core-js/fn/string/includes';
import 'core-js/es7/array'; 

// for ie10, map is undefined , set is undefined
import 'core-js/es6/map';
import 'core-js/es6/set';

import React from 'react';
import ReactDOM from 'react-dom';
import '../stylesheets/index.css';
import App from './components/App';

//Disable pinch zoom in mobile browser
function touchHandler(event){
  if(event.touches.length > 1){
      //the event is multi-touch
      //you can then prevent the behavior
      event.preventDefault()
  }
}

document.addEventListener("touchmove", touchHandler, {passive: false});

ReactDOM.render(<App />,
  document.getElementById('app')
);
