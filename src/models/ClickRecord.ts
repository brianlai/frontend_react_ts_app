//TODO: should share this type definition with app.ts in backend
export type ClickRecord = { second: number, blueBtnClickCount: number; orangeBtnClickCount: number };
export type UserClickRecord = {
    userId: string,
    clickRecords: ClickRecord[]
}

export type ChartingUserClickRecord = {
  second: string,
  blueBtnClickCount: number,
  orangeBtnClickCount: number,
  clicks: number,
}

export type ChartingUserClickRecords = ChartingUserClickRecord[]