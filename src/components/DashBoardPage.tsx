import React, {useState, useRef, ChangeEvent} from "react";
import "../../stylesheets/dashboard-page.css"
import * as constants from "../constants"

import {useLocation} from "react-router-dom"
import qs from "query-string"

// import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Label, TooltipPayload, TooltipProps} from 'recharts';

import highcharts from "highcharts";

import { ApolloClient, InMemoryCache, ApolloProvider, useQuery, useSubscription, gql} from '@apollo/client';
import { WebSocketLink } from '@apollo/client/link/ws';

import {ClickRecord, UserClickRecord, ChartingUserClickRecord, ChartingUserClickRecords} from "../models/ClickRecord";

const wsLink = new WebSocketLink({
  uri: `ws://${constants.hostName}:${constants.backendPort}/graphql`,
  options: {
    reconnect: true
  }
});

const client = new ApolloClient({
  link: wsLink,
  // uri: constants.backendGraphQlUrl,
  cache: new InMemoryCache(),
})

const _data: ChartingUserClickRecord[] = [
  { second: "0", clicks: 0, blueBtnClickCount: 0, orangeBtnClickCount:0},
];

const DashBoardpage: React.FC = () => {
  let location = useLocation()
  let {userId} = qs.parse(location.search)
  // let userId = "az0k5mdfzvpk8w7l9njx9g";
  let [userClickRecord, setUserClickRecord] = useState({} as UserClickRecord);
  // const inputEl = useRef<HTMLInputElement>(null);
  const orangeClickCountEl = useRef<HTMLDivElement>(null);
  const blueClickCountEl = useRef<HTMLDivElement>(null);

  // let currentWidth = document.body.clientWidth
  let currentWidth = document.querySelector(".container")? document.querySelector(".container")!.clientWidth: 0;
  let graphWidth = currentWidth > 800? 800: currentWidth;

  console.log(`userId: ${userId}`)

  const SUB_USER_CLICKCOUNT = gql`
  subscription {
    userClickRecord(userId: "${userId}") {
      userId
      clickRecords {
        second,
        blueBtnClickCount,
        orangeBtnClickCount
      }
    }
  }
  `;

  const subscribeAndPlotUserClickRecord = (plotFn) => {
    const {data} = useSubscription(SUB_USER_CLICKCOUNT);
    // debugger
    if (!data) {
      return null
    }

    console.log(data.userClickRecord)

    var _data = data.userClickRecord as UserClickRecord

    let orangeBtnClickTotalCount = (_data.clickRecords).reduce((accu, ele)=>accu + ele.orangeBtnClickCount, 0)
    let blueBtnClickTotalCount = (_data.clickRecords).reduce((accu, ele)=>accu + ele.blueBtnClickCount, 0)

    debugger
    if(orangeClickCountEl && orangeClickCountEl.current) {
      orangeClickCountEl.current.innerHTML = String(orangeBtnClickTotalCount)
    }

    if(blueClickCountEl && blueClickCountEl.current) {
      blueClickCountEl.current.innerHTML = String(blueBtnClickTotalCount)
    }

    // setUserClickRecord(data.userClickRecord)
    plotFn(_data)

    return data
  }

  const plotGraph = (userClickRecord: UserClickRecord) => {
    if (!userClickRecord) {
      return
    }

    let clickRecords = userClickRecord?.clickRecords
    console.log("clickrecords: ", clickRecords)

    let newClickRecords: ChartingUserClickRecords = [{
      second: "0",
      blueBtnClickCount: 0,
      orangeBtnClickCount: 0,
      clicks: 0,
    }, ...clickRecords.map((record)=>{
      return {
        ...record,
        second: String(record.second),
        clicks: record.blueBtnClickCount + record.orangeBtnClickCount
      }
    })]

    let plotData = newClickRecords
    let totalClickSeries = plotData.map(data=>data.clicks)
    let blueClickSeries = plotData.map(data=>data.blueBtnClickCount)
    let orangeClickSeries = plotData.map(data=>data.orangeBtnClickCount)
      
    highcharts.chart("chart-container", {
        title: {text: ""},
        yAxis: {
          title: {
              text: 'Clicks(s)'
          }
        },
    
        xAxis: {
          title: {
            text: 'Second(s)'
          }
        },
        legend: {enabled: true},
        tooltip: {
          useHTML: true,
          // formatter: function() {},
          formatter: function (arg) {
            let label = String(this.x);
            let tooltipCorrespondingDatas = plotData.filter((data)=>data.second == label)

            // console.log(tooltipCorrespondingDatas)

            if(tooltipCorrespondingDatas.length !== 1) {
              console.error("tooltipCorrespondingDatas.length !== 1")
            }

            let tooltipCorrespondingData:ChartingUserClickRecord = tooltipCorrespondingDatas[0]

            return `<div class="my-tooltip">
              <div class="total-clicks">total : ${tooltipCorrespondingData.clicks}</div>
              <div class="orange-clicks">orange : ${tooltipCorrespondingData.orangeBtnClickCount}</div>
              <div class="blue-clicks">blue : ${tooltipCorrespondingData.blueBtnClickCount}</div>
            </div>`
          }
        },
        series: [
          /* {
            name: "total clicks",
            data: totalClickSeries,
            animation: false, 
            color: "black"
          }, */
          {
            name: "blue clicks",
            data: blueClickSeries,
            animation: false, 
            color: "blue"
          },
          {
            name: "orange clicks",
            data: orangeClickSeries,
            animation: false, 
            color: "orange"
          },
        ],
      } as highcharts.Options);
  };

  subscribeAndPlotUserClickRecord(plotGraph);

  // return <div>This is the dashboard page</div>
  return (
    <div className="container">
      <div role="group">
        <div>
          <div className="dashboard">
            <div className="header center">Your results</div>
            {/* {
              (()=>{
                return <input ref={inputEl} type="checkbox" onChange={onShowAccumulativeCheckboxClicked}/>
              })()
            }
            <span>Show Accumlative Count</span> */}

            <div className="row">
                {/* <LineChart className="center" width={graphWidth} height={400} data={plotData} margin={{ top: 5, right: 20, bottom: 5, left: 0 }}>
                    <Line type="monotone" dataKey="clicks" stroke="#8884d8" />
                    <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
                    <XAxis dataKey="second">
                        <Label value="Seconds" offset={0} position="insideBottom" />
                    </XAxis>
                    <YAxis allowDecimals={false}>
                    <Label value="Clicks" offset={0} angle={-90} position="insideLeft" />
                    </YAxis>
                    <Tooltip content={<CustomTooltip/>}/>
                </LineChart> */}
                <div id="chart-container" style={{width: `${graphWidth}px`}}></div>
            </div>
            <div className="row">
              <div className="box orange box1" ref={orangeClickCountEl}>0</div>
              <div className="box blue box2" ref={blueClickCountEl}>0</div>
            </div>
            <div className="row footer">
              {/* <div className="black center">
                Please go to{" "}
                <a href={`${constants.frontendBaseUrl}/client`}>
                {`${constants.frontendBaseUrl}/client`}
                </a>{" "}
                to join the game.
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default () => (
  <ApolloProvider client={client}>
    <DashBoardpage></DashBoardpage>
  </ApolloProvider>);
