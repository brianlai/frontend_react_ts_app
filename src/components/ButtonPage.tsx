import React, {useState} from "react"
import "../../stylesheets/button-page.css"

import { useHistory } from "react-router-dom";

// import {backendGraphQlUrl, frontendBaseUrl} from "../constants"
import * as constants from "../constants"

type ClickCountRecord = {blueBtnClickCount: number, orangeBtnClickCount: number,}
let clickCounts: ClickCountRecord[] = []
let blueBtnClickCount = 0, orangeBtnClickCount = 0;
let currentSecond = 0;

const secondsLimit: number|undefined = undefined

//https://stackoverflow.com/a/13403498
function generateQuickGuid() {
  return Math.random().toString(36).substring(2, 15) +
      Math.random().toString(36).substring(2, 15);
}

const ButtonPage:React.FC = () => {
  let alertMsgs = ["Try to click either of the orange/blue buttons. Result will be shown in the Dashboard page",
    "Counting will start on the first click."]

  let [stateAlertMsgs, setStateAlertMsg] = useState(alertMsgs);
  let history = useHistory();
  let [userId, setUserId] = useState("")
  
  if(userId === "") {
    let userId = generateQuickGuid()
    // let userId = "az0k5mdfzvpk8w7l9njx9g";
    console.log(`UUID: ${userId}`)
    setUserId(userId)
  }

  const oranageBtnOnClickHandler = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
    if(clickCounts.length == 0) {
      startCountDown();
    }
    
    ++orangeBtnClickCount;
    console.log("Orange btn is clicked")
  }
  
  const blueBtnOnClickHandler = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
    if(clickCounts.length == 0) {
      startCountDown();
    }
  
    ++blueBtnClickCount;
    console.log("Blue btn is clicked")
  }
  
  const startCountDown = () => {
    console.log("Starting count down ...")
    
    
    blueBtnClickCount = 0
    orangeBtnClickCount = 0
    clickCounts = [{blueBtnClickCount, orangeBtnClickCount}]
  
    scheduleLogRecursive(1, ()=>{
      

      setStateAlertMsg([`It's over! Here is your result ...`]);
      setTimeout(()=> {
        //use react router history.push() to go to dashboard page
        // history.push(`${constants.frontendBasePath}/dashboard?userId=${encodeURI(userId)}`)

        //redirect to the dash board
        window.location.href = `${constants.frontendBaseUrl}/dashboard?userId=${encodeURI(userId)}`
      }, 1000)
    })
  }
  
  const scheduleLogRecursive = function(second:number=1, completeCallback?: ()=>void){
    // setStateAlertMsg([`Counting down - ${5-currentSecond}s ...`]);
    setStateAlertMsg([`Counting seconds - ${currentSecond}s ...`]);
    setTimeout(()=>{
      ++currentSecond;
      // setStateAlertMsg([`Counting down - ${5-currentSecond}s ...`]);
      setStateAlertMsg([`Counting seconds - ${currentSecond}s ...`]);
      clickCounts.push({blueBtnClickCount, orangeBtnClickCount});
      console.log(clickCounts);

      //TODO: issue WS request to save result in backend
      // var myHeaders = new Headers();
      // myHeaders.append("Content-Type", "application/json");

      var graphqlRequestBody = JSON.stringify({
        query: `mutation {
          addUserClickRecord (userClickRecordInput: {
            userId: "${userId}"
            clickRecord: {
              second: ${second},
              blueBtnClickCount: ${blueBtnClickCount},
              orangeBtnClickCount: ${orangeBtnClickCount}
            }
          }) {
            userId
            clickRecords {
              second
              blueBtnClickCount
              orangeBtnClickCount
            }
          }
        }`,
      })
      var requestOptions: RequestInit = {
        method: 'POST',
        // headers: myHeaders,
        headers: {
          "Content-Type": "application/json"
        },
        body: graphqlRequestBody,
        redirect: 'follow'
      };

      fetch(constants.backendGraphQlUrl, requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));
  
      blueBtnClickCount = 0
      orangeBtnClickCount = 0
  
      if(!secondsLimit || currentSecond < secondsLimit) {
        scheduleLogRecursive(++second, completeCallback);
      } else {
        if(completeCallback) {
          completeCallback()
        }
      }
    }, 1000)  
  }
  
  return (
    <div className="container">
      <div className="alert alert-primary my-message col-sm-12 center noselect" role="alert">
        {
          (() => {
            return stateAlertMsgs.map((msg, i)=>{
              return <div key={i}>{msg}</div>
            })
          })()
        }
        
      </div>
      <div role="group">
        <div className="row">
          <div className="col-sm-6 center button-wrapper">
            <button
              className="button round orange my-button noselect"
              onClick={oranageBtnOnClickHandler}
              style={{outline: "none"}}
            >
              -
            </button>
          </div>
          <div className="col-sm-6 center">
            <button
              className="button round blue noselect"
              onClick={blueBtnOnClickHandler}
              style={{outline: "none"}}
            >
              +
            </button>
          </div>
        </div>
        <div className="dashboard-link"><a href={`${constants.frontendBaseUrl}/dashboard?userId=${encodeURI(userId)}`} target="_blank">Open your Dashboard page</a></div>
      </div>
    </div>
  );
};

export default ButtonPage