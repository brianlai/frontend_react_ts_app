import React from 'react';
import logo from '../../assets/logo.svg';
import ButtonPage from "./ButtonPage";
import DashBoardPage from "./DashBoardPage";
import 'bootstrap/dist/css/bootstrap.css';
import * as constants from "../constants"

import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch
} from 'react-router-dom';

function App() {
  return (<Router>
    <div className="App">
      {/* Hellow World from App
      <button className="btn-primary">hello1</button> */}

      <Switch>
        <Route exact path={`${constants.frontendBasePath}/client`}>
          <ButtonPage></ButtonPage>
        </Route>
        <Route exact path={`${constants.frontendBasePath}/dashboard`}>
          <DashBoardPage></DashBoardPage>
        </Route>
      </Switch>
      {/* <img src={logo}/> */}
    </div>
    </Router>
  );
}

export default App


