const webpack = require("webpack");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const path = require("path");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');



module.exports = {
    // mode: "development",
    entry: "./src/index.tsx",
    output: {
        filename: "index.bundle.js",
        path: path.resolve(__dirname, "dist"),
        publicPath: "/app/"
    },
    // devtool: "inline-source-map",
    devServer: {
        contentBase: "src/",
        historyApiFallback: true,
    },
    module: {
        rules: [
            /* {
                test: /\.(tsx|ts)$/,
                use: "ts-loader",
                exclude: /node_modules/
            }, */
            {
                test: /\.(js|ts|tsx)?/,
                // include: [SRC_DIR, DESIGN_DIR, PDF_JS_PATH, PDF_JS_VIEWER_PATH],
                use: {
                    loader: "babel-loader",
                    options: {
                        // presets: ["react", "es2015", "stage-2"]
                        presets: ["@babel/preset-react", "@babel/preset-env", "@babel/preset-typescript"]
                    }
                }
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
                /* use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                localIdentName: '[name]_[local]--[hash:base64:5]',
                            },
                        },
                    }], */
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader'
                    }
                ]
            },
        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            PRODUCTION: JSON.stringify(true),
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new MiniCssExtractPlugin(),
        // new BundleAnalyzerPlugin(),
        
    ],
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".svg"]
    },
    optimization: {
        // minimize: false,
        minimize: true,
        minimizer: [
            /*new UglifyJsPlugin({
                uglifyOptions: {
                    sourceMap: false,
                    compress: {
                        sequences: true,
                        dead_code: true,
                        conditionals: true,
                        booleans: true,
                        unused: true,
                        if_return: true,
                        join_vars: true,
                        drop_console: true
                    },
                    /!*mangle: {
                        except: ['$super', '$', 'exports', 'require']
                    },*!/
                    output: {
                        comments: false
                    }
                },
            }),*/
            new TerserPlugin({
                                 terserOptions: {
                                     output: {
                                         comments: false,
                                     },
                                 },
                                 extractComments: false,
                             })
        ],
        //split index.bundle.js & vendors.bundle.js
        /* splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all'
                }
            }
        } */
    },
}